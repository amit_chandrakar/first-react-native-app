import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Button, Alert, Image } from 'react-native';

export default function App() {

  const onPressLearnMore = () => {
    alert('sample alert');
  }


  return (
    <View style={styles.container}>
      
      <ActivityIndicator size="small" color="green" />

      <Button
        title="Button1"
        color="green"
        onPress={ () => Alert.alert('hello world') }
        accessibilityLabel="Learn more about this purple button"
        // disabled
        testID="amit123"
      >

      </Button>

      <Image
          style={styles.logo}
          source={{uri: 'https://snack-web-player.s3.us-west-1.amazonaws.com/v2/40/static/media/react-native-logo.79778b9e.png'}}
        />


      <Text>AMIT KUMAR CHANDRAKAR fcghvjb</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 66,
    height: 58,
  },
});
